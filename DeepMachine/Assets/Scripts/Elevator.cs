﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public GameObject player;
    public GameObject elevatorBottom;
    public Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        elevatorBottom = GameObject.Find("ElevatorBottom");
    }

    private void OnTriggerEnter(Collider other)
    {
        player.transform.position = elevatorBottom.transform.position;
    }
}
