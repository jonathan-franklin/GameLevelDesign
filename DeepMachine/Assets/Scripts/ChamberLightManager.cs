﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ChamberLightManager : MonoBehaviour
{
    UnityEvent lightSwitch;
    Light light;
    GameObject[] lamps;

    // Start is called before the first frame update
    void Start()
    {
        if (lightSwitch == null)
        {
            lightSwitch = new UnityEvent();
        }

        lightSwitch.AddListener(Switch);

        lamps = GameObject.FindGameObjectsWithTag("ChamberLight");
    }

    private void OnTriggerEnter(Collider other)
    {
        lightSwitch.Invoke();
    }

    private void Switch()
    {
        foreach (GameObject Light in lamps)
        {
            light = Light.GetComponent<Light>();
            light.enabled = !light.enabled;
        }
    }
}
